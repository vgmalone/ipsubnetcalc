#!/usr/bin/python3

switcher = {
        '8' : '255.0.0.0',
        '9' : '255.128.0.0',
        '10': '255.192.0.0',
        '11': '255.224.0.0',
        '12': '255.240.0.0',
        '13': '255.248.0.0',
        '14': '255.252.0.0',
        '15': '255.254.0.0',
        '16': '255.255.0.0',
        '17': '255.255.128.0',
        '18': '255.255.192.0',
        '19': '255.255.224.0',
        '20': '255.255.240.0',
        '21': '255.255.248.0',
        '22': '255.255.252.0',
        '23': '255.255.254.0',
        '24': '255.255.255.0',
        '25': '255.255.255.128',
        '26': '255.255.255.192',
        '27': '255.255.255.224',
        '28': '255.255.255.240',
        '29': '255.255.255.248',
        '30': '255.255.255.252'
        }


def information():
    print('''

        ******  Private IP Address Calculator  ******

        This calculator will help you determine what
        subnets, host range, and number of useable hosts
        can be used on a private LAN.


        ''')

    b_list = range(16,32)
    correct_class = False
    while not correct_class:
        pclass = input("Enter Class (A, B, or C): ").upper()
        if pclass == 'A':
            correct_class = True       
        elif pclass == 'B':
            correct_class = True
        elif pclass == 'C':
            correct_class = True
        else:
            print("Enter a correct Private IP Class!")
    
    b_list = range(16,32)
    correct_ip = False
    while not correct_ip:
        address = input("Enter IP Address: ")
        ip4 = address.split(".")
        if ip4[0] == '10':
            correct_ip = True
        elif ip4[0] == '172' and int(ip4[1]) in b_list:
            correct_ip = True
        elif ip4[0] == '192' and ip4[1] == '168':
            correct_ip = True
        else:
            print("Enter a correct {} Class IP address".format(pclass))             

    slash = input("Enter Subnet: /")
    ip4.append(slash)
    return ip4, pclass

def class_A(oct2,cidr):
    mp = {
            8 : 256,
            9 : 128,
            10 : 64,
            11 : 32,
            12 : 16,
            13 : 8,
            14 : 4,
            15 : 2
            }
    x = mp[cidr] - 1
    for i in range(0,256,mp[cidr]):
        if oct2 in range(i, i + mp[cidr]):
            print("      Network: 10.{}.0.0".format(str(i)))
            print("    Broadcast: 10.{}.255.255".format(str(i + x)))
            print(" Usable Hosts:", (256 * 256 * mp[cidr])-2)


def class_B(oct3,cidr):
    mp = {
            16: 256,
            17: 128,
            18: 64,
            19: 32,
            20: 16,
            21: 8,
            22: 4,
            23: 2
            }
    x = mp[cidr] - 1
    for i in range(0,256,mp[cidr]):
        if oct3 in range(i, i + mp[cidr]):
            print("      Network: {}.{}.{}.0".format(ip4[0],ip4[1], str(i)))
            print("    Broadcast: {}.{}.{}.255".format(ip4[0],ip4[1], str(i + x)))
            print(" Usable Hosts:", (256 * mp[cidr])-2)
            break
        else:
            continue

def class_C(oct4, cidr):
    mp = {
            24: 256,
            25: 128,
            26: 64,
            27: 32,
            28: 16,
            29: 8,
            30: 4
            }
    x = mp[cidr] -1
    for i in range(0,256,mp[cidr]):
        if oct4 in range(i, i + mp[cidr]):
            print("      Network: {}.{}.{}.{}".format(ip4[0],ip4[1],ip4[2], str(i)))
            print("    Broadcast: {}.{}.{}.{}".format(ip4[0],ip4[1],ip4[2], str(i + x)))
            print(" Usable Hosts:", (mp[cidr])-2)
            break
        else:
            continue


#  =============================================================================

ip4, pclass = information()
print('\n' * 2)
print(f"        Class: {pclass}")
print(f"   IP Address: {ip4[0]}.{ip4[1]}.{ip4[2]}.{ip4[3]}")
print("      Netmask:", switcher[ip4[4]])

if int(ip4[4]) >= 8 and int(ip4[4]) < 16:
    class_A(int(ip4[1]),int(ip4[4]))
elif int(ip4[4]) >= 16 and int(ip4[4]) < 24:
    class_B(int(ip4[2]),int(ip4[4]))
else:
    class_C(int(ip4[3]), int(ip4[4]))













   
